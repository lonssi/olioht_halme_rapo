/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 *
 * @author 0403231 & 0418527
 */
public class FXMLStorageWindowController implements Initializable {

    Main main = null;

    @FXML
    private ListView<String> itemList;
    @FXML
    private ListView<String> sizeList;
    @FXML
    private ListView<String> weightList;
    @FXML
    private ListView<String> classList;
    @FXML
    private ListView<String> startList;
    @FXML
    private ListView<String> endList;
    @FXML
    private ListView<String> distanceList;
    @FXML
    private Button refreshButton;
    @FXML
    private Button closeButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        main = Main.getInstance();

        fillTable();

    }

    @FXML
    private void refreshAction(ActionEvent event) {

        itemList.getItems().clear();
        sizeList.getItems().clear();
        weightList.getItems().clear();
        classList.getItems().clear();
        startList.getItems().clear();
        endList.getItems().clear();
        distanceList.getItems().clear();

        fillTable();

    }

    @FXML
    private void closeAction(ActionEvent event) {

        // Close the window
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();

    }
    
    private void fillTable() {
        for (Package p : main.Storage().getPackages()) {
            itemList.getItems().add(p.getItem().getName());
            sizeList.getItems().add(String.format("%.0f",p.getItem().getSize()));  
            weightList.getItems().add(String.format("%.2f", p.getItem().getWeight()));
            classList.getItems().add(String.valueOf(p.getPackageClass()));
            startList.getItems().add(p.getFromDispenser().getPostoffice());
            endList.getItems().add(p.getTargetDispenser().getPostoffice());
            distanceList.getItems().add(String.format("%.0f",p.getDistance()));

        }
    }

}
