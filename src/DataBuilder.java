
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 0403231 & 0418527
 */
public class DataBuilder {

    private ArrayList<SmartPost> smartPostAL = new ArrayList();
    private ArrayList<City> cityAL = new ArrayList();

    static private DataBuilder db = null;

    // Singleton
    static public DataBuilder getInstance() {
        if (db == null) {
            db = new DataBuilder();
        }
        return db;
    }

    // Initialization of the SmartPost list from XML
    public void setSmartPosts() {
        try {
            URL url = new URL("http://smartpost.ee/fi_apt.xml");

            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String content = "";
            String line;
            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
            SmartPostXMLGet spxml = new SmartPostXMLGet();
            smartPostAL = spxml.getData(content);

        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<SmartPost> getSmartPosts() {
        return smartPostAL;
    }

    // Gets list of all cities as SmartPost objects
    public ArrayList<SmartPost> getCities() {

        ArrayList<SmartPost> cl = new ArrayList();
        int s = getSmartPosts().size();

        // Copy the SmartPost ArrayList
        for (int i = 0; i < s; i++) {
            cl.add(getSmartPosts().get(i));
        }

        // Delete SmartPosts so only one remains from each city
        for (int i = 0; i < s; i++) {
            for (int j = s - 1; j > i; j--) {
                if (cl.get(i).getCity().equals(cl.get(j).getCity())) {
                    cl.remove(j);
                    s--;
                }
            }
        }
        return cl;
    }

    // Builds list of all cities as City objects
    public void setCityList() {

        ArrayList<SmartPost> cl;
        cl = new ArrayList();
        cl = getCities();
        for (SmartPost s : cl) {
            City c = new City(s.getCity());
            cityAL.add(c);
        }
    }

    // Gets list of all cities as City objects
    public ArrayList<City> getCityList() {
        return cityAL;
    }

    // Gets list off all SmartPosts in a city
    public ArrayList<SmartPost> getSmartPostsPerCity(String city) {

        ArrayList<SmartPost> smpc = new ArrayList();
        for (SmartPost s : getSmartPosts()) {
            if (s.getCity().equals(city)) {
                smpc.add(s);
            }
        }
        return smpc;
    }

    // Calculates distance between two points according to coordinates
    // Source: http://www.geodatasource.com/developers/java
    public double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public void initializeFile() {

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("log.txt"));

            Date today = new Date();
            SimpleDateFormat dFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
            String date = dFormat.format(today);
            out.write(date + "\r\n\r\n");
            out.close();

        } catch (Exception e) {

        }

    }

    public void writeFile(String msg) {

        try {

            File file = new File("log.txt");

            if (!file.exists()) {
                file.createNewFile();

            } else {
                
                FileWriter fileWritter = new FileWriter(file.getName(), true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);

                Date today = new Date();
                SimpleDateFormat dFormat = new SimpleDateFormat("HH:mm:ss");
                String date = dFormat.format(today);

                bufferWritter.write(date + "\t" + msg + "\r\n");
                bufferWritter.close();
                
            }

        } catch (Exception e) {

        }

    }

}
