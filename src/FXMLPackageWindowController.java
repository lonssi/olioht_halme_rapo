/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author 0403231 & 0418527
 */
public class FXMLPackageWindowController implements Initializable {

    private Main main = null;

    private static Package p;

    @FXML
    private ComboBox<Item> itemBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private CheckBox fragileCheckBox;
    @FXML
    private RadioButton firstClassRadio;
    @FXML
    private RadioButton secondClassRadio;
    @FXML
    private RadioButton thirdClassRadio;
    @FXML
    private Button infoButton;
    @FXML
    private ComboBox<City> fromCityBox;
    @FXML
    private ComboBox<SmartPost> fromDispenserBox;
    @FXML
    private ComboBox<City> targetCityBox;
    @FXML
    private ComboBox<SmartPost> targetDispenserBox;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private TextArea console;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        main = Main.getInstance();

        // Initialization of the comboboxes
        for (City s : main.DataBuilder().getCityList()) {
            fromCityBox.getItems().add(s);
        }

        for (City s : main.DataBuilder().getCityList()) {
            targetCityBox.getItems().add(s);
        }

        for (Item i : main.Storage().getItems()) {
            itemBox.getItems().add(i);
        }

        itemBox.valueProperty().setValue(new Item("(tyhjä)", 0, 0, false));

    }

    @FXML
    private void infoAction(ActionEvent event) {

        String message;
        message = "\n1. Luokka\n"
                + "\tLähetysmatka korkeintaan 150km\n"
                + "\tSärkyvät tavarat menevät rikki\n"
                + "\tNopein toimitus\n"
                + "2. Luokka\n"
                + "\tToiseksi nopein toimitus\n"
                + "\tMaksimikoko esineelle on 27000cm²\n"
                + "3. Luokka\n"
                + "\tSärkyvät tavarat menevät rikki\n"
                + "\tHitain toimitus\n";
        console.appendText(message);

    }

    @FXML
    private void cancelAction(ActionEvent event) {

        // Close the window
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();

    }

    @FXML
    private void createPackageAction(ActionEvent event) {

        try {

            Item item;

            if (!itemBox.valueProperty().getValue().getName().equals("(tyhjä)")) {

                // If a prebuilt item is selected
                item = itemBox.valueProperty().getValue();

            } else {

                // If the user chooses to create a new item
                String size = sizeField.getText();
                String tokens[] = size.split("\\*");

                int num1 = Integer.parseInt(tokens[0]);
                int num2 = Integer.parseInt(tokens[1]);
                int num3 = Integer.parseInt(tokens[2]);

                int size_int = num1 * num2 * num3;
                String name = nameField.getText();
                double weight = Double.parseDouble(massField.getText());
                boolean fragile = fragileCheckBox.isSelected();

                item = new Item(name, size_int, weight, fragile);

            }

            // Check the selected package class
            if (firstClassRadio.isSelected()) {
                p = new FirstClass();
            } else if (secondClassRadio.isSelected()) {
                p = new SecondClass();
            } else if (thirdClassRadio.isSelected()) {
                p = new ThirdClass();
            } else {
                throw new Exception();
            }

            // Check if the route is established
            if ((fromDispenserBox.valueProperty().getValue() != null)
                    && (targetDispenserBox.valueProperty().getValue() != null)) {
                p.addRouteInfo(fromDispenserBox.valueProperty().getValue(),
                        targetDispenserBox.valueProperty().getValue());
            } else {
                throw new Exception();
            }

            String str = p.checkIfLegit(fromDispenserBox.valueProperty().getValue(),
                    targetDispenserBox.valueProperty().getValue(), item);

            // Check if the item breaks any rules of the package
            if (!str.equals("")) {
                consoleMsg(str);
                return;
            }

            // No constrains were violated, add item to package
            p.insertItem(item);

            // Add package to storage if there's still room
            if (main.Storage().getSize() < 20) {
                main.Storage().addPackage(p);
            } else {
                consoleMsg("Varasto on täynnä!");
                return;
            }

            consoleMsg("Esineen " + p.getItem().getName() + " sisältävä "
                    + p.getPackageClass() + ". luokan paketti luotu!");

        } catch (Exception e) {
            consoleMsg("Paketin tiedot ovat virheelliset tai puutteelliset!");
        }

    }

    @FXML
    private void fromCityBoxValueChange(ActionEvent event) {

        fromDispenserBox.getItems().clear();

        for (SmartPost s : main.DataBuilder().
                getSmartPostsPerCity(fromCityBox.valueProperty().getValue().getName())) {
            fromDispenserBox.getItems().add(s);
        }

        fromDispenserBox.setValue(null);

    }

    @FXML
    private void targetCityBoxValueChange(ActionEvent event) {

        targetDispenserBox.getItems().clear();

        for (SmartPost s : main.DataBuilder().
                getSmartPostsPerCity(targetCityBox.valueProperty().getValue().getName())) {
            targetDispenserBox.getItems().add(s);
        }

        targetDispenserBox.setValue(null);

    }

    @FXML
    private void fromDispenserBoxValueChange(ActionEvent event) {
    }

    @FXML
    private void targetDispenserBoxValueChange(ActionEvent event) {
    }

    private void consoleMsg(String str) {

        // Add also to log file
        main.DataBuilder().writeFile(str);

        str += "\n";
        console.appendText(str);

    }

    @FXML
    private void onFirstClassRadioValueChange(ActionEvent event) {
        if (firstClassRadio.isSelected()) {
            secondClassRadio.setSelected(false);
            thirdClassRadio.setSelected(false);
        }
    }

    @FXML
    private void onSecondClassRadioValueChange(ActionEvent event) {
        if (secondClassRadio.isSelected()) {
            firstClassRadio.setSelected(false);
            thirdClassRadio.setSelected(false);
        }
    }

    @FXML
    private void onThirdClassRadioValueChange(ActionEvent event) {
        if (thirdClassRadio.isSelected()) {
            firstClassRadio.setSelected(false);
            secondClassRadio.setSelected(false);
        }
    }

}
