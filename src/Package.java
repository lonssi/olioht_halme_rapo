/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 0403231 & 0418527
 */
public abstract class Package {

    public Item item;
    public int range;
    public int packageClass;
    public int size;

    public SmartPost fromDispenser;
    public SmartPost targetDispenser;

    public double distance;

    public Package() {
    }

    public void insertItem(Item i) {
        item = i;
    }

    public void addRouteInfo(SmartPost fA, SmartPost tA) {
        fromDispenser = fA;
        targetDispenser = tA;
        setDistance();

    }

    public String toString() {
        return item.getName() + " " + packageClass + ". luokka";
    }

    public Item getItem() {
        return item;
    }

    public int getRange() {
        return range;
    }

    public int getSize() {
        return size;
    }

    public int getPackageClass() {
        return packageClass;
    }

    public SmartPost getFromDispenser() {
        return fromDispenser;
    }

    public SmartPost getTargetDispenser() {
        return targetDispenser;
    }

    public String checkIfLegit(SmartPost a, SmartPost b, Item i) {
        return "";
    }

    public void setDistance() {
        Main main = Main.getInstance();
        distance = main.DataBuilder().distance(fromDispenser.getLocation().getLatitude(),
                fromDispenser.getLocation().getLongitude(),
                targetDispenser.getLocation().getLatitude(),
                targetDispenser.getLocation().getLongitude());
    }

    public double getDistance() {
        return distance;
    }

    public String getColor() {
        if (packageClass == 1) {
            return "red";
        } else if (packageClass == 2) {
            return "blue";
        } else {
            return "green";
        }
    }

}

class FirstClass extends Package {

    public FirstClass() {
        range = 150;
        size = Integer.MAX_VALUE;
        packageClass = 1;
    }

    @Override
    public String checkIfLegit(SmartPost a, SmartPost b, Item i) {

        Main main = Main.getInstance();

        if (getRange() < distance) {
            return "Liian pitkä välimatka!";
        } else if (i.isFragile()) {
            return "Esine menee rikki kuljetuksen aikana!";
        }
        return "";
    }
}

class SecondClass extends Package {

    public SecondClass() {
        range = Integer.MAX_VALUE;
        size = 30 * 30 * 30;
        packageClass = 2;
    }

    @Override
    public String checkIfLegit(SmartPost a, SmartPost b, Item i) {
        if (i.getSize() > getSize()) {
            return "Esine on liian suuri!";
        }
        return "";
    }

}

class ThirdClass extends Package {

    public ThirdClass() {
        range = Integer.MAX_VALUE;
        size = Integer.MAX_VALUE;
        packageClass = 3;
    }

    @Override
    public String checkIfLegit(SmartPost a, SmartPost b, Item i) {
        if (i.isFragile()) {
            return "Esine menee rikki kuljetuksen aikana!";
        }
        return "";
    }

}
