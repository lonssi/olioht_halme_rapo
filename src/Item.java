/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 0403231 & 0418527
 */
public class Item {

    private String name;
    private int size;
    private double weight;
    private boolean fragile;
    private boolean bomb;

    public Item(String n, int s, double w, boolean f) {
        name = n;
        size = s;
        weight = w;
        fragile = f;
        bomb = false;
    }
    
    public void setBomb() {
        bomb = true;
    }
    
    public boolean getBomb() {
        return bomb;
    }

    public String getName() {
        return name;
    }

    public double getSize() {
        return size;
    }

    public double getWeight() {
        return weight;
    }

    public boolean isFragile() {
        return fragile;
    }
    
    public String toString() {
        return name;
    }

}
