/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 0403231 & 0418527
 */
public class Main {

    static private Main main = null;

    public Main() {
    }

    // Singleton
    static public Main getInstance() {
        if (main == null) {
            main = new Main();
        }
        return main;
    }

    // Allows access to storage
    public Storage Storage() {
        return Storage.getInstance();
    }

    // Allows access to databuilder
    public DataBuilder DataBuilder() {
        return DataBuilder.getInstance();
    }

}
