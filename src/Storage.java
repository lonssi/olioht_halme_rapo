import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 0403231 & 0418527
 */
public class Storage {

    static private Storage storage = null;

    ArrayList<Package> packages = new ArrayList();
    ArrayList<Item> items = new ArrayList();

    // Singleton
    static public Storage getInstance() {
        if (storage == null) {
            storage = new Storage();
        }
        return storage;
    }

    public void addPackage(Package p) {
        packages.add(p);
    }

    public ArrayList<Package> getPackages() {
        return packages;
    }

    public void addItem(Item i) {
        items.add(i);
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void deletePackage(Package a) {
        for (Package i : packages) {
            if (a.getItem().getName().equals(i.getItem().getName())) {
                packages.remove(a);
                break;
            }
        }
    }
    
    public int getSize() {
        return packages.size();
    }

}
