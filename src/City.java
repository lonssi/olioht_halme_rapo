/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 0403231 & 0418527
 */
public class City {
    
    private String name;
    
    public City(String n) {
        name = n;
    }
    
    public String getName() {
        return name;
    }
    
    public String toString() {
        return name;
    }
    
}
