import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 0403231 & 0418527
 */
public class SmartPostXMLGet {

    private Document doc;
    private ArrayList<SmartPost> al;

    public ArrayList<SmartPost> getData(String content) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();

            al = new ArrayList();

            parseCurrentData();

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

        return al;
    }

    private void parseCurrentData() {

        NodeList nodes = doc.getElementsByTagName("place");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            // Extract info from XML
            String code = e.getElementsByTagName("code").item(0).getFirstChild().getNodeValue();
            String city = e.getElementsByTagName("city").item(0).getFirstChild().getNodeValue();
            String address = e.getElementsByTagName("address").item(0).getFirstChild().getNodeValue();
            String postoffice = e.getElementsByTagName("postoffice").item(0).getFirstChild().getNodeValue();
            String availability = e.getElementsByTagName("availability").item(0).getFirstChild().getNodeValue();

            String lat = e.getElementsByTagName("lat").item(0).getFirstChild().getNodeValue();
            String lon = e.getElementsByTagName("lng").item(0).getFirstChild().getNodeValue();

            // Conversion
            Double lat_d = Double.parseDouble(lat);
            Double lon_d = Double.parseDouble(lon);
            
            
            // String formatting
            String[] availability_p = availability.split(",");
            availability = "";
            
            for (String str : availability_p) {
                availability += str + "<br>";
            }

            // Create SmartPost object and add to list
            al.add(new SmartPost(code, city, address, postoffice, availability, lat_d, lon_d));

        }
    }
}
