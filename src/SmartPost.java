/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 0403231 & 0418527
 */
class SmartPost {

    private String code;
    private String city;
    private String address;
    private String postoffice;
    private String availability;
    private LocationData location;

    public SmartPost(String co, String ci, String a, String p,
            String av, double lat, double lon) {
        
        code = co;
        city = ci;
        address = a;
        postoffice = p.substring(19);
        availability = av;
        location = new LocationData(lat, lon);

    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public String getAvailability() {
        return availability;
    }

    public LocationData getLocation() {
        return location;
    }

    public String toString() {
        return postoffice;
    }

    // Subclass containing location information
    public class LocationData {

        private double latitude;
        private double longitude;

        public LocationData(double lat, double lon) {
            latitude = lat;
            longitude = lon;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }

}
