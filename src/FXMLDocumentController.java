/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author 0403231 & 0418527
 */
public class FXMLDocumentController implements Initializable {

    private Main main = null;

    private URL soundToPlay = getClass().getResource("1.wav");
    private AudioClip AC = Applet.newAudioClip(soundToPlay);

    @FXML
    private WebView web;
    @FXML
    private ComboBox<City> dispenserBox;
    @FXML
    private Button addToMapButton;
    @FXML
    private Button deletePathsButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private ComboBox<Package> packageBox;
    @FXML
    private Button refreshPackageButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button storageButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        sendPackageButton.setDisable(false);

        main = Main.getInstance();

        // Initialize text log file
        main.DataBuilder().initializeFile();

        // Get all smartposts from XML
        main.DataBuilder().setSmartPosts();
        main.DataBuilder().setCityList();

        // Add all cities in the combobox
        for (City s : main.DataBuilder().getCityList()) {
            dispenserBox.getItems().add(s);
        }

        // Load the map
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());

        // Let's create some prebuilt items
        main.Storage().addItem(new Item("(tyhjä)", 0, 0, false));

        main.Storage().addItem(new Item("Pokemonkorttipakka", 8, 0.1, false));
        main.Storage().addItem(new Item("Hieromasauva", 2000, 0.5, false));
        main.Storage().addItem(new Item("Posliininorsu", 1000000, 15, true));
        main.Storage().addItem(new Item("Sydänsiirrännäinen", 1000, 1, true));
        main.Storage().addItem(new Item("Emu", 2000000, 40, true));
        Item bomb = new Item("Pommi (allahu akbar)", 200, 1, true);
        bomb.setBomb();
        main.Storage().addItem(bomb);

    }

    @FXML
    private void addToMapAction(ActionEvent event) {

        // Get the corresponding city name from the combobox
        String city = dispenserBox.valueProperty().getValue().getName();

        // Get all SmartPosts in one city and place the dots on the map
        for (SmartPost s : main.DataBuilder().getSmartPostsPerCity(city)) {
            String command = "document.goToLocation('<";
            command += s.getAddress() + ">, <";
            command += s.getCode() + "> <";
            command += s.getCity() + ">', '";
            command += s.getPostoffice() + "<br>";
            command += s.getAvailability() + "', 'red')";

            web.getEngine().executeScript(command);
        }

    }

    @FXML
    private void deletePathsButton(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void createPackageAction(ActionEvent event) {

        try {
            Stage packageWindow = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageWindow.fxml"));
            Scene scene = new Scene(page);

            page.setStyle("-fx-background-image: url('bg1.jpg');"
                    + " -fx-background-repeat: stretch;"
                    + " -fx-background-size: stretch;"
                    + " -fx-background-position: center center;");

            packageWindow.setScene(scene);
            packageWindow.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void refreshPackageAction(ActionEvent event) {

        packageBox.getItems().clear();

        for (Package p : main.Storage().getPackages()) {
            packageBox.getItems().add(p);
        }

    }

    @FXML
    private void sendPackageAction(ActionEvent event) {

        if (packageBox.valueProperty().getValue() != null) {

            SmartPost startPost = packageBox.valueProperty().getValue().getFromDispenser();
            SmartPost endPost = packageBox.valueProperty().getValue().getTargetDispenser();

            String command = "";

            double[] loc = new double[4];

            loc[0] = startPost.getLocation().getLatitude();
            loc[1] = startPost.getLocation().getLongitude();
            loc[2] = endPost.getLocation().getLatitude();
            loc[3] = endPost.getLocation().getLongitude();

            command += "document.createPath(";
            command += "[" + loc[0] + ", " + loc[1] + ", "
                    + loc[2] + ", " + loc[3] + "], '"
                    + packageBox.valueProperty().getValue().getColor() + "', "
                    + packageBox.valueProperty().getValue().getPackageClass() + ")";

            // Draw the route on the map
            web.getEngine().executeScript(command);

            main.DataBuilder().writeFile("Esineen "
                    + packageBox.valueProperty().getValue().getItem().getName()
                    + " sisältävä "
                    + packageBox.valueProperty().getValue().getPackageClass()
                    + ". luokan paketti on lähetetty automaatista "
                    + startPost.getPostoffice() + " automaattiin "
                    + endPost.getPostoffice() + ".");

            if (packageBox.valueProperty().getValue().getItem().getBomb()) {
                AC.play();
            }

            // Delete package from storage
            main.Storage().deletePackage(packageBox.valueProperty().getValue());

            // Update the combobox
            packageBox.getItems().clear();
            for (Package p : main.Storage().getPackages()) {
                packageBox.getItems().add(p);
            }

            packageBox.valueProperty().setValue(null);

        }

    }

    @FXML
    private void openStorageAction(ActionEvent event) {

        try {
            Stage packageWindow = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLStorageWindow.fxml"));
            Scene scene = new Scene(page);

            page.setStyle("-fx-background-image: url('bg1.jpg');"
                    + " -fx-background-repeat: stretch;"
                    + " -fx-background-size: stretch;"
                    + " -fx-background-position: center center;");

            packageWindow.setScene(scene);
            packageWindow.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
